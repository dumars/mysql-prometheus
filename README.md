# mysql 效能監控與 consul 服務註冊

使用 mysql exporter 來取得 mysql 的狀態，並加入 consul 讓 mysql 加入服務註冊機制，並使用 Prometheus 來儲存監控資料以及使用 Grafana 顯示即時監控圖表。

```shell script
# 啟動 container
docker-compose up -d

# 停止並刪除 container
docker-compose down
```

1. 使用瀏覽器打開 [consul services](http://localhost:8500/ui/dc1/services) 畫面

2. 使用瀏覽器打開 [prometheus](http://localhost:9090) 畫面

3. 使用瀏覽器打開 [grafana](http://localhost:3000) 畫面，帳號密碼為 admin/admin，選擇名稱為 MySQL-Prometheus 的 dashboard